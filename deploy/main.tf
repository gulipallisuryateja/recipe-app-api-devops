terraform {
  backend "s3" {
    bucket         = "terraform-remote-backend-buckt"
    key            = "recipe-app.tfstate"
    region         = "ap-south-1"
    encrypt        = true
    dynamodb_table = "s3-statefile-lock"
  }
}

provider "aws" {
  region  = "ap-south-1"
  version = "~> 3.21.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
data "aws_region" "current" {}

